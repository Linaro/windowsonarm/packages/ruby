#!/usr/bin/env bash

set -euo pipefail
set -x

pacman -S --noconfirm --needed autoconf
pacman -S --noconfirm --needed ${MINGW_PACKAGE_PREFIX}-ruby \
                               ${MINGW_PACKAGE_PREFIX}-libyaml \
                               ${MINGW_PACKAGE_PREFIX}-openssl

which ruby
ruby --version

if [ ! -f .configured ]; then
    autoreconf -fi
    ./configure --with-baseruby=$(which ruby)
    touch .configured
fi

make -j$(nproc)
make test -j$(nproc) || true

file ruby.exe
./ruby.exe --version
./ruby.exe -e 'puts "Hello world"'

