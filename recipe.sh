#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

script_dir=$(dirname $(readlink -f $0))

msys_script=

msys_run()
{
    $msys_script "$@"
}

msys_prepare()
{
    msys_script=$(pwd)/msys2_clangarm64.sh
    if [ -f msys2_clangarm64.sh ]; then return; fi
    wget https://gitlab.com/Linaro/windowsonarm/packagetools/-/raw/master/scripts/msys2_clangarm64.sh
    chmod +x msys2_clangarm64.sh
}

checkout()
{
    if [ -d ruby ]; then return; fi

    git clone https://github.com/ruby/ruby
    pushd ruby
    git checkout $version
    popd
}

build()
{
    pushd ruby
    cp $script_dir/build_ruby.sh .
    msys_run ./build_ruby.sh
    popd
}

msys_prepare
checkout
build
